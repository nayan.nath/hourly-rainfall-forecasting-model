"""

model_forecastor.fit(t_any_data,t+1_any_data)

model_rainfall_calculator.fit(t_any_data,t_rainfall_data)


t+1_any_output = model_forecastor.predict(t_any_data)

t+1_rainfall_output = model_rainfall_calculator.predict(t+1_any_output)


t+1_rainfall_output = some_model.predict(t_any_data)


"""
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, explained_variance_score
from sklearn.neural_network import  MLPRegressor as regr
from sklearn.ensemble import BaggingRegressor
np.random.seed(0)
from keras.models import Sequential
from keras.layers import Dense
import keras
from sklearn.feature_selection import mutual_info_regression, SelectKBest
from sklearn.decomposition import KernelPCA



def get_dataset():
    """
    Get the original raw data
    :return: rainfall_dataframe
    """

    rainfall_dataframe = pd.read_csv('update_dataset/raw_dataset.csv', sep=',')

    return rainfall_dataframe

def data_selection(upper_limit, lower_limit):
    """
    Selection of data with lower limit and upper limit.
    :param upper_limit: highest value of the data-set
    :param lower_limit: lowest value of the data-set
    :return: X_, y_
    """
    rainfall_dataframe=get_dataset()

    flg = 0
    '''Select the features for move one columns to 24 hours columns '''
    for j in range(5,23):
        '''Taking all hourly data 0 to 23'''
        for i in range(24):
            ''' select all the features in the hour column where hour value is i '''
            index_for_ith_hour_feature = np.where(rainfall_dataframe.iloc[:, 3] == i)[0]
            '''if the flag value 1 it will enter the loop for making one column to 24 hour columns'''
            if flg == 1:
                '''taking all the data where jth value will be constant and 24 hours features will be selected and stacked horizontally'''
                buffer = rainfall_dataframe.iloc[index_for_ith_hour_feature, j].values.reshape(-1,1)
                '''stack all the data with buffer'''
                all_data = np.hstack((all_data, buffer))
            else:
                '''first loop will comes here because flg=0, after that, it will go to if loop and continue'''
                all_data = rainfall_dataframe.iloc[index_for_ith_hour_feature, j].values.reshape(-1,1)
                flg = 1


    '''Select date columns'''
    date_data_column = rainfall_dataframe.iloc[index_for_ith_hour_feature, 0:3].values


    '''stacking date columns and all the features previously selected'''
    all_data = np.hstack((date_data_column,all_data))

    temperature = all_data[:, 19].reshape(-1, 1)
    relative_humidity = all_data[:, 43].reshape(-1, 1)
    mean_sea_label_pressure = all_data[:, 67].reshape(-1, 1)
    total_cloud_cover = all_data[:, 123].reshape(-1, 1)
    high_cloud_cover = all_data[:, 163].reshape(-1, 1)
    medium_cloud_cover = all_data[:, 187].reshape(-1, 1)
    low_cloud_cover = all_data[:, 211].reshape(-1, 1)
    sunshine_duration = all_data[:, 235].reshape(-1, 1)
    shortwave_radiation = all_data[:, 259].reshape(-1, 1)
    wind_speed_10m_above_gnd = all_data[:, 283].reshape(-1, 1)
    wind_direction_10m_above_gnd = all_data[:, 307].reshape(-1, 1)
    wind_speed_80m_above_gnd = all_data[:, 331].reshape(-1, 1)
    wind_direction_80m_above_gnd = all_data[:, 355].reshape(-1, 1)
    wind_speed = all_data[:, 379].reshape(-1, 1)
    wind_direction_900mb = all_data[:, 403].reshape(-1, 1)
    wind_gust_900mb = all_data[:, 427].reshape(-1, 1)

    X = np.hstack(
        (date_data_column, temperature, relative_humidity, mean_sea_label_pressure, total_cloud_cover, high_cloud_cover,
         medium_cloud_cover, low_cloud_cover, sunshine_duration,
         shortwave_radiation, wind_speed_10m_above_gnd, wind_direction_10m_above_gnd,
         wind_speed_80m_above_gnd, wind_direction_80m_above_gnd,
         wind_speed, wind_direction_900mb, wind_gust_900mb))


    y = all_data[:, 91].reshape(-1, 1)

    '''find the indices for which the specified limits hold true'''
    non_zero_idx_1 = np.where(y > lower_limit)
    non_zero_idx_2 = np.where(y < upper_limit)

    '''find the indices the satisfy both condition'''
    non_zero_idx = np.intersect1d((non_zero_idx_1), (non_zero_idx_2))

    '''only select the rows for the indices found above'''
    X_ = X[non_zero_idx, :]
    y_ = y[non_zero_idx].reshape(-1, 1)


    return X_, y_

def plot_mi(X,y):
    """
    Get all the important features from all dataset.
    :param X: Train dataset
    :param y: Test dataset
    :return: mutual info regression plot
    """
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    xc = np.arange(X.shape[1])
    ax.bar(xc, mutual_info_regression(X, y))
    ax.set_xlabel('All Features', **axis_font)
    ax.set_ylabel('Mutual Info Regression Score',**axis_font)
    ax.set_title('MI Info Regression Score(all features)',**title_font)
    plt.show()

def plot_total_rainfall(all_data):



    """

    :param all_data:
    :return: plot total test datset
    """
    aggr=[]
    x_l = []
    for i in range(75,99):
        aggr.append(len(np.where(all_data[:, i] > 0)[0]))
        x_l.append(str(i-75)+"h")


    plt.bar(x_l,aggr)

    plt.xlabel("Time of observations")
    plt.ylabel("Number of days when it rained")
    plt.title('Total Rainfall')
    plt.show()

def plot_target_rainfall(y):
    """

    :param y:
    :return: plot total average rainfall results in graph
    """
    plt.plot(y)
    plt.xlabel("Number of Observations")
    plt.ylabel("Temperature at one observation(mm)")
    plt.title("Temperature Prediction")
    plt.grid()
    plt.show()

def scaling_operation(do_scaling, X, y):
    """
    :param do_scaling: True or False
    :param X:
    :param y:
    :return: Scaled total train data
    """

    if do_scaling:
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

        scaler_x_std = StandardScaler()
        scaler_x_std.fit(X_train)
        X_train_std = scaler_x_std.transform(X_train)
        X_test_std = scaler_x_std.transform(X_test)


        scaler_x_minmax=MinMaxScaler()


        scaler_x_minmax.fit(X_train_std)
        X_train_scaled=scaler_x_minmax.transform(X_train_std)

        X_test_scaled=scaler_x_minmax.transform(X_test_std)


        scaler_y_std = StandardScaler()
        scaler_y_std.fit(y_train)
        y_train_std = scaler_y_std.transform(y_train)

        scaler_y_minmax = MinMaxScaler()
        scaler_y_minmax.fit(y_train_std)
        y_train_scaled=scaler_y_minmax.transform(y_train_std)



        return scaler_y_std, scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test

    else:

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

        return None, None, X_train, X_test, y_train, y_test

def train_model(X,y,use_keras=False):
    """
    :param X:
    :param y:
    :param use_keras:
    :return:
    """
    """
    num_layers = 2
    num_neurons = 130
    """
    num_layers = 2
    num_neurons = 130

    activation='relu'
    regularization_factor = 1e-2
    learning_rate_init = 1e-4
    alpha = 0.01
    n_epochs = 200
    batch_size = 100

    if use_keras:
        keras.backend.clear_session()

        ''' Select a optimizer '''
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)
        #optimizer = keras.optimizers.Adamax(lr=learning_rate_init)

        ''' Initialize a sequential/ feed forward model'''
        regressor = Sequential()

        ''' add input and first hidden layer'''
        regressor.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        ''' add subsequent hidden layer'''
        for _ in range(num_layers-1):
            regressor.add(Dense(units = num_neurons,
                                activation = activation,
                               )
                         )
        ''' add output layer'''
        regressor.add(Dense(units=y.shape[1], activation='relu'))

        ''' complile the regressor'''
        regressor.compile(optimizer=optimizer, loss='mse', metrics=['accuracy'])

        '''train the model, varbose ( Print the result in the screen)'''

        history = regressor.fit(X, y, epochs=n_epochs, batch_size=batch_size, verbose=1)

        '''plot the results'''

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)


        ax_loss.plot(history.history['loss'])
        #plt.plot(history.history['val_loss'])
        ax_loss.set_title('model loss; num_layers:'+str(num_layers)+',num_neurons:'+str(num_neurons)+', activation:'+str(activation)+', regularization_factor:'+str(regularization_factor)+', learning_rate_init:'+str(learning_rate_init)+', batch_size:'+str(batch_size))
        ax_loss.set_ylabel('loss')
        ax_loss.set_xlabel('epoch')
        ax_loss.grid()
        plt.show()

    else:

        optimizer = 'adam'
        layers = [num_neurons] * num_layers
        learning_rate_strategy = 'constant'
        nn_regressor = regr(hidden_layer_sizes=layers,
                            activation=activation,
                            solver=optimizer,
                            alpha=regularization_factor,
                            learning_rate=learning_rate_strategy,
                            learning_rate_init=learning_rate_init,
                            batch_size=batch_size, verbose=True)

        regressor = BaggingRegressor(nn_regressor, n_estimators=10)

        for _ in range(n_epochs):
            regressor.fit(X, y)


    return regressor

def get_metrics(true, predicted):

    error = true - predicted
    pe = (error) / true

    mape = np.mean(np.abs(pe)) * 100
    print('Mean Absolute Percentage Error : {}'.format(mape))

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    evs = explained_variance_score(true, predicted)
    print('Explained Variance Score: {}'.format(evs))

    r2_s = r2_score(y_true=true, y_pred=predicted)
    print('R2 Score : {}'.format(r2_s))

    plt.hist(error,50)
    # plt.xlim(-20,20, 0, 0.3)
    plt.xlabel('Variance')
    plt.ylabel('Number of observations')
    plt.title('Histogram of Error')
    plt.grid(True)
    plt.show()

    return mape, mae, rmse, evs

def plot_results(true, predicted, scores):

    plt.style.use('seaborn-darkgrid')
    plt.plot(true, label='Actual')
    plt.plot(predicted, color='indianred', label='Predicted')
    plt.xlabel('Number of Observation')
    plt.ylabel('Rainfall(mm)')
    plt.title("Rainfall Prediction; MAPE:"+ str(round(scores[0],3))+" MAE:"+str(round(scores[1],3))+" RMSE:"+str(round(scores[2],3))+" EVS:"+str(round(scores[3], 3)))
    plt.legend()
    plt.show()

def main():

    do_scaling = True

    # for 16h rainfall
    lower_limit = -np.inf #-1
    upper_limit = np.inf #3
    X, y = data_selection(upper_limit, lower_limit)
    scaler_y_std, scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test = scaling_operation(do_scaling, X, y)


    # Mutual Info Regression
    plot_mi(X, y)


if __name__ == "__main__":
    main()
