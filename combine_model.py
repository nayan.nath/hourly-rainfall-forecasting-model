# Import Libraries
import numpy as np
import pandas as pd
import itertools
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, explained_variance_score, f1_score, confusion_matrix
# Classifier
from sklearn.ensemble import GradientBoostingClassifier as clf
# Calculator
from sklearn.ensemble import GradientBoostingRegressor as regr
np.random.seed(0)
from keras.models import Sequential
from keras.layers import Dense
import keras
from sklearn.feature_selection import mutual_info_regression, SelectKBest
from joblib import dump, load



def get_dataset():
    """
    Get the original raw data
    :return: rainfall_dataframe1 and rainfall_dataframe2
    """
    rainfall_dataframe1 = pd.read_csv('update_dataset/dataset111.csv', sep=',')
    rainfall_dataframe2 = pd.read_csv('update_dataset/dataset222.csv', sep=',')

    return rainfall_dataframe1, rainfall_dataframe2

def data_selection_predictor(upper_limit_predictor, lower_limit_predictor, var=16):
    """
    Selection of data with lower limit and upper limit.
    :param upper_limit: set up the highest value at total precipitation data
    :param lower_limit: set up the lowest value of the data-set
    :return: X_, y_
    """

    rainfall_dataframe1, rainfall_dataframe2 = get_dataset()

    # Select date columns(day, month, year)
    date_data_column = rainfall_dataframe1.iloc[:, 0:3].values
    # Select feature columns
    features_data_column = rainfall_dataframe1.iloc[:, 4:].values
    # Stack date columns and all the features previously selected
    all_data = np.hstack((date_data_column, features_data_column))

    relative_humidity = all_data[:, 27:43]
    mean_sea_label_pressure = all_data[:, 51:67]
    low_cloud_cover = all_data[:, 195:211]
    sunshine_duration = all_data[:, 219:235]
    shortwave_radiation = all_data[:, 243:259]
    wind_direction_10m_above_gnd = all_data[:, 291:307]
    wind_speed = all_data[:, 363:379]

    X = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                   shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))

    if var == 16:
        # Index feature selection at 16 hour
        relative_humidity = all_data[:, 43].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 67].reshape(-1, 1)
        low_cloud_cover = all_data[:, 211].reshape(-1, 1)
        sunshine_duration = all_data[:, 235].reshape(-1, 1)
        shortwave_radiation = all_data[:, 259].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 307].reshape(-1, 1)
        wind_speed = all_data[:, 379].reshape(-1, 1)
        total_precipitation = all_data[:, 91].reshape(-1, 1)
    elif var == 17:
        # Index feature selection at 17 hour
        relative_humidity = all_data[:, 44].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 68].reshape(-1, 1)
        low_cloud_cover = all_data[:, 212].reshape(-1, 1)
        sunshine_duration = all_data[:, 236].reshape(-1, 1)
        shortwave_radiation = all_data[:, 260].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 308].reshape(-1, 1)
        wind_speed = all_data[:, 380].reshape(-1, 1)
        total_precipitation = all_data[:, 92].reshape(-1, 1)
    elif var == 18:
        # Index feature selection at 18 hour
        relative_humidity = all_data[:, 45].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 69].reshape(-1, 1)
        low_cloud_cover = all_data[:, 213].reshape(-1, 1)
        sunshine_duration = all_data[:, 237].reshape(-1, 1)
        shortwave_radiation = all_data[:, 261].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 309].reshape(-1, 1)
        wind_speed = all_data[:, 381].reshape(-1, 1)
        total_precipitation = all_data[:, 93].reshape(-1, 1)
    elif var == 19:
        # Index feature selection at 19 hour
        relative_humidity = all_data[:, 46].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 70].reshape(-1, 1)
        low_cloud_cover = all_data[:, 214].reshape(-1, 1)
        sunshine_duration = all_data[:, 238].reshape(-1, 1)
        shortwave_radiation = all_data[:, 262].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 310].reshape(-1, 1)
        wind_speed = all_data[:, 382].reshape(-1, 1)
        total_precipitation = all_data[:, 94].reshape(-1, 1)
    elif var == 20:
        # Index feature selection at 20 hour
        relative_humidity = all_data[:, 47].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 71].reshape(-1, 1)
        low_cloud_cover = all_data[:, 215].reshape(-1, 1)
        sunshine_duration = all_data[:, 239].reshape(-1, 1)
        shortwave_radiation = all_data[:, 263].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 311].reshape(-1, 1)
        wind_speed = all_data[:, 383].reshape(-1, 1)
        total_precipitation = all_data[:, 95].reshape(-1, 1)
    elif var ==21:
        # Index feature selection at 21 hour
        relative_humidity = all_data[:, 48].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 72].reshape(-1, 1)
        low_cloud_cover = all_data[:, 216].reshape(-1, 1)
        sunshine_duration = all_data[:, 240].reshape(-1, 1)
        shortwave_radiation = all_data[:, 264].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 312].reshape(-1, 1)
        wind_speed = all_data[:, 384].reshape(-1, 1)
        total_precipitation = all_data[:, 96].reshape(-1, 1)
    elif var ==22:
        # Index feature selection at 22 hour
        relative_humidity = all_data[:, 49].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 73].reshape(-1, 1)
        low_cloud_cover = all_data[:, 217].reshape(-1, 1)
        sunshine_duration = all_data[:, 241].reshape(-1, 1)
        shortwave_radiation = all_data[:, 265].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 313].reshape(-1, 1)
        wind_speed = all_data[:, 385].reshape(-1, 1)
        total_precipitation = all_data[:, 97].reshape(-1, 1)
    else:
        # Index feature selection at 23 hour
        relative_humidity = all_data[:, 50].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 74].reshape(-1, 1)
        low_cloud_cover = all_data[:, 218].reshape(-1, 1)
        sunshine_duration = all_data[:, 242].reshape(-1, 1)
        shortwave_radiation = all_data[:, 266].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 314].reshape(-1, 1)
        wind_speed = all_data[:, 386].reshape(-1, 1)
        total_precipitation = all_data[:, 98].reshape(-1, 1)

    y = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                   shortwave_radiation, wind_direction_10m_above_gnd, wind_speed, total_precipitation))


    # Find the indices for which specified the limits hold true
    non_zero_idx_1 = np.where(y[:, -1] > lower_limit_predictor)
    non_zero_idx_2 = np.where(y[:, -1] < upper_limit_predictor)

    # Find the indices the satisfy both condition
    non_zero_idx = np.intersect1d((non_zero_idx_1), (non_zero_idx_2))

    # Only select the rows for the indices found above
    X_ = X[non_zero_idx, :]
    y_ = y[non_zero_idx, :]

    return X_, y_

def data_selection_calculator(upper_limit_for_predictions_data, lower_limit_for_predictions_data, var =16):
    """
    Selection of data with lower limit and upper limit.
    :param upper_limit: set up the highest value at total precipitation data
    :param lower_limit: set up the lowest value of the data-set
    :return: X, y
    """
    rainfall_dataframe1, rainfall_dataframe2 = get_dataset()

    # Select date columns(day, month, year)
    date_data_column = rainfall_dataframe2.iloc[:, 0:3].values
    # Select features columns
    features_data_column = rainfall_dataframe2.iloc[:, 4:].values

    # Stack date columns and all the features previously selected
    all_data = np.hstack((date_data_column, features_data_column))

    if var == 16:
        # Index feature selection at 16 hour
        relative_humidity = all_data[:, 43].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 67].reshape(-1, 1)
        low_cloud_cover = all_data[:, 211].reshape(-1, 1)
        sunshine_duration = all_data[:, 235].reshape(-1, 1)
        shortwave_radiation = all_data[:, 259].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 307].reshape(-1, 1)
        wind_speed = all_data[:, 379].reshape(-1, 1)
        total_precipitation = all_data[:, 91].reshape(-1, 1)
    elif var == 17:
        # Index feature selection at 17 hour
        relative_humidity = all_data[:, 44].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 68].reshape(-1, 1)
        low_cloud_cover = all_data[:, 212].reshape(-1, 1)
        sunshine_duration = all_data[:, 236].reshape(-1, 1)
        shortwave_radiation = all_data[:, 260].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 308].reshape(-1, 1)
        wind_speed = all_data[:, 380].reshape(-1, 1)
        total_precipitation = all_data[:, 92].reshape(-1, 1)
    elif var == 18:
        # Index feature selection at 18 hour
        relative_humidity = all_data[:, 45].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 69].reshape(-1, 1)
        low_cloud_cover = all_data[:, 213].reshape(-1, 1)
        sunshine_duration = all_data[:, 237].reshape(-1, 1)
        shortwave_radiation = all_data[:, 261].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 309].reshape(-1, 1)
        wind_speed = all_data[:, 381].reshape(-1, 1)
        total_precipitation = all_data[:, 93].reshape(-1, 1)
    elif var == 19:
        # Index feature selection at 19 hour
        relative_humidity = all_data[:, 46].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 70].reshape(-1, 1)
        low_cloud_cover = all_data[:, 214].reshape(-1, 1)
        sunshine_duration = all_data[:, 238].reshape(-1, 1)
        shortwave_radiation = all_data[:, 262].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 310].reshape(-1, 1)
        wind_speed = all_data[:, 382].reshape(-1, 1)
        total_precipitation = all_data[:, 94].reshape(-1, 1)
    elif var == 20:
        # Index feature selection at 20 hour
        relative_humidity = all_data[:, 47].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 71].reshape(-1, 1)
        low_cloud_cover = all_data[:, 215].reshape(-1, 1)
        sunshine_duration = all_data[:, 239].reshape(-1, 1)
        shortwave_radiation = all_data[:, 263].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 311].reshape(-1, 1)
        wind_speed = all_data[:, 383].reshape(-1, 1)
        total_precipitation = all_data[:, 95].reshape(-1, 1)
    elif var == 21:
        # Index feature selection at 21 hour
        relative_humidity = all_data[:, 48].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 72].reshape(-1, 1)
        low_cloud_cover = all_data[:, 216].reshape(-1, 1)
        sunshine_duration = all_data[:, 240].reshape(-1, 1)
        shortwave_radiation = all_data[:, 264].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 312].reshape(-1, 1)
        wind_speed = all_data[:, 384].reshape(-1, 1)
        total_precipitation = all_data[:, 96].reshape(-1, 1)
    elif var == 22:
        # Index feature selection at 22 hour
        relative_humidity = all_data[:, 49].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 73].reshape(-1, 1)
        low_cloud_cover = all_data[:, 217].reshape(-1, 1)
        sunshine_duration = all_data[:, 241].reshape(-1, 1)
        shortwave_radiation = all_data[:, 265].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 313].reshape(-1, 1)
        wind_speed = all_data[:, 385].reshape(-1, 1)
        total_precipitation = all_data[:, 97].reshape(-1, 1)
    else:
        # Index feature selection at 23 hour
        relative_humidity = all_data[:, 50].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 74].reshape(-1, 1)
        low_cloud_cover = all_data[:, 218].reshape(-1, 1)
        sunshine_duration = all_data[:, 242].reshape(-1, 1)
        shortwave_radiation = all_data[:, 266].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 314].reshape(-1, 1)
        wind_speed = all_data[:, 386].reshape(-1, 1)
        total_precipitation = all_data[:, 98].reshape(-1, 1)

    X = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                   shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))

    y = total_precipitation

    # Find the indices for which the specified limits hold true
    zero_idx = np.where(y[:, 0] <= lower_limit_for_predictions_data)[0]
    upper_limit_violating_index = np.where(y[:, 0] > upper_limit_for_predictions_data)[0]

    # Only select the rows for the indices found above
    y[zero_idx, :] = 1e-3
    y[upper_limit_violating_index, :] = np.mean(y)



    """
    # Increase the data over and over time for testing how is it performing
    flag = 0

    for i in range(24):
        relative_humidity = all_data[:, 27+i].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 51+i].reshape(-1, 1)
        low_cloud_cover = all_data[:, 195+i].reshape(-1, 1)
        sunshine_duration = all_data[:, 219+i].reshape(-1, 1)
        shortwave_radiation = all_data[:, 243+i].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 291+i].reshape(-1, 1)
        wind_speed = all_data[:, 363+i].reshape(-1, 1)
        total_precipitation = all_data[:, 75+i].reshape(-1, 1)

        if flag == 0:
            X = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                           shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))
            y = total_precipitation
            flag = 1
        else:
            buff_x = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                                shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))
            buff_y = total_precipitation


            X = np.vstack((X, buff_x))
            y = np.vstack((y, buff_y))

    """

    return X, y

def data_selection_classifier(var =16):
    """
    Selection of data with lower limit and upper limit.
    :param upper_limit: set up the highest value at total precipitation data
    :param lower_limit: set up the lowest value of the data-set
    :return: X_chosen, y_chosen
    """
    rainfall_dataframe1, rainfall_dataframe2 = get_dataset()

    # Select date columns(day, month, year)
    date_data_column = rainfall_dataframe2.iloc[:, 0:3].values
    # Select feature columns
    features_data_column = rainfall_dataframe2.iloc[:, 4:].values
    # Stack date columns and all the features previously selected
    all_data = np.hstack((date_data_column, features_data_column))


    if var == 16:
        # Index feature selection at 16 hour
        relative_humidity = all_data[:, 43].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 67].reshape(-1, 1)
        low_cloud_cover = all_data[:, 211].reshape(-1, 1)
        sunshine_duration = all_data[:, 235].reshape(-1, 1)
        shortwave_radiation = all_data[:, 259].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 307].reshape(-1, 1)
        wind_speed = all_data[:, 379].reshape(-1, 1)
        total_precipitation = all_data[:, 91].reshape(-1, 1)
    elif var == 17:
        # Index feature selection at 17 hour
        relative_humidity = all_data[:, 44].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 68].reshape(-1, 1)
        low_cloud_cover = all_data[:, 212].reshape(-1, 1)
        sunshine_duration = all_data[:, 236].reshape(-1, 1)
        shortwave_radiation = all_data[:, 260].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 308].reshape(-1, 1)
        wind_speed = all_data[:, 380].reshape(-1, 1)
        total_precipitation = all_data[:, 92].reshape(-1, 1)
    elif var == 18:
        # Index feature selection at 18 hour
        relative_humidity = all_data[:, 45].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 69].reshape(-1, 1)
        low_cloud_cover = all_data[:, 213].reshape(-1, 1)
        sunshine_duration = all_data[:, 237].reshape(-1, 1)
        shortwave_radiation = all_data[:, 261].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 309].reshape(-1, 1)
        wind_speed = all_data[:, 381].reshape(-1, 1)
        total_precipitation = all_data[:, 93].reshape(-1, 1)
    elif var == 19:
        # Index feature selection at 19 hour
        relative_humidity = all_data[:, 46].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 70].reshape(-1, 1)
        low_cloud_cover = all_data[:, 214].reshape(-1, 1)
        sunshine_duration = all_data[:, 238].reshape(-1, 1)
        shortwave_radiation = all_data[:, 262].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 310].reshape(-1, 1)
        wind_speed = all_data[:, 382].reshape(-1, 1)
        total_precipitation = all_data[:, 94].reshape(-1, 1)
    elif var == 20:
        # Index feature selection at 20 hour
        relative_humidity = all_data[:, 47].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 71].reshape(-1, 1)
        low_cloud_cover = all_data[:, 215].reshape(-1, 1)
        sunshine_duration = all_data[:, 239].reshape(-1, 1)
        shortwave_radiation = all_data[:, 263].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 311].reshape(-1, 1)
        wind_speed = all_data[:, 383].reshape(-1, 1)
        total_precipitation = all_data[:, 95].reshape(-1, 1)
    elif var == 21:
        # Index feature selection at 21 hour
        relative_humidity = all_data[:, 48].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 72].reshape(-1, 1)
        low_cloud_cover = all_data[:, 216].reshape(-1, 1)
        sunshine_duration = all_data[:, 240].reshape(-1, 1)
        shortwave_radiation = all_data[:, 264].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 312].reshape(-1, 1)
        wind_speed = all_data[:, 384].reshape(-1, 1)
        total_precipitation = all_data[:, 96].reshape(-1, 1)
    elif var == 22:
        # Index feature selection at 22 hour
        relative_humidity = all_data[:, 49].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 73].reshape(-1, 1)
        low_cloud_cover = all_data[:, 217].reshape(-1, 1)
        sunshine_duration = all_data[:, 241].reshape(-1, 1)
        shortwave_radiation = all_data[:, 265].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 313].reshape(-1, 1)
        wind_speed = all_data[:, 385].reshape(-1, 1)
        total_precipitation = all_data[:, 97].reshape(-1, 1)
    else:
        # Index feature selection at 23 hour
        relative_humidity = all_data[:, 50].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 74].reshape(-1, 1)
        low_cloud_cover = all_data[:, 218].reshape(-1, 1)
        sunshine_duration = all_data[:, 242].reshape(-1, 1)
        shortwave_radiation = all_data[:, 266].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 314].reshape(-1, 1)
        wind_speed = all_data[:, 386].reshape(-1, 1)
        total_precipitation = all_data[:, 98].reshape(-1, 1)

    X_ = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                   shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))

    y = total_precipitation
    """
    # Increase the data over and over time for testing how is it performing
    flag = 0

    for i in range(17):
        relative_humidity = all_data[:, 27 + i].reshape(-1, 1)
        mean_sea_label_pressure = all_data[:, 51 + i].reshape(-1, 1)
        low_cloud_cover = all_data[:, 195 + i].reshape(-1, 1)
        sunshine_duration = all_data[:, 219 + i].reshape(-1, 1)
        shortwave_radiation = all_data[:, 243 + i].reshape(-1, 1)
        wind_direction_10m_above_gnd = all_data[:, 291 + i].reshape(-1, 1)
        wind_speed = all_data[:, 363 + i].reshape(-1, 1)
        total_precipitation = all_data[:, 75 + i].reshape(-1, 1)

        if flag == 0:
            X = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                           shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))
            y = total_precipitation
            flag = 1
        else:
            buff_x = np.hstack((relative_humidity, mean_sea_label_pressure, low_cloud_cover, sunshine_duration,
                                shortwave_radiation, wind_direction_10m_above_gnd, wind_speed))
            buff_y = total_precipitation

            X = np.vstack((X, buff_x))
            y = np.vstack((y, buff_y))
    """


    """
    ''' Biased Dataset for Confusion Matrix--> Classifier Accuracy(F1_Score)-->0.6935064935064935'''
    ''' Thresholding operation on total precipitation '''
    lower_limit_violating_idx = np.where(y[:, 0] > 0)[0]

    y[lower_limit_violating_idx, 0] = 1

    y_ = y.astype(int)
    
    
    """

    # Thresholding operation at total precipitation index
    non_zero_idx = np.where(y[:, 0] > 0)[0]
    # If it's greater then zero set it to 1
    y[non_zero_idx, 0] = 1
    # Store it as a integer
    y_ = y.astype(int)
    # Select all the index from total precipitation where value is zero
    zero_idx = np.where(y_[:, 0] == 0)[0]
    # Choose minimum number of samples from  X and y
    num_of_samples_to_choose = min(len(non_zero_idx), len(zero_idx))
    # Choose minimum numner of observation from X and y and the zero index from total precipitation which value hold zero
    chosen_zero_idx = np.random.choice(zero_idx, num_of_samples_to_choose)
    #Select index from minimum axis which is non zero or zero
    chosen_non_zero_idx = np.random.choice(non_zero_idx, num_of_samples_to_choose)

    all_chosen_idx = np.hstack((chosen_zero_idx, chosen_non_zero_idx))
    y_chosen = y_[all_chosen_idx, 0].reshape(-1,1)
    X_chosen = X_[all_chosen_idx, :]

    return X_chosen, y_chosen

def plot_mi(X, y):
    """
    Get all the important features from all dataset.
    :param X: Train features
    :param y: Target feature
    :return: mutual info regression plot
    """

    fig_mi = plt.figure()
    ax = fig_mi.add_subplot(1, 1, 1)
    xc = np.arange(X.shape[1])

    ax.bar(xc, mutual_info_regression(X, y))
    plt.show()

def plot_total_rainfall(all_data):
    """

    :param all_data:
    :return: plot total test data set
    """
    aggr = []
    x_l = []
    for i in range(75, 99):
        aggr.append(len(np.where(all_data[:, i] > 0)[0]))
        x_l.append(str(i - 75) + "h")

    plt.bar(x_l, aggr)

    plt.xlabel("Time of observations")
    plt.ylabel("Number of days when it rained")
    plt.title('Total Rainfall')
    plt.show()

def plot_target_rainfall(y):
    """

    :param y:
    :return: plot total average hourly rainfall results in graph
    """
    plt.plot(y)
    plt.xlabel("Number of Observations")
    plt.ylabel("Rainfall at one observation(mm)")
    plt.title(" Hourly Rainfall")
    plt.grid()
    plt.show()

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """

    :param cm:
    :param classes:
    :param normalize:
    :param title: Set up your title
    :param cmap:
    :return: Confusion matrix for classifier
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    ax_loss.set_title(title, **title_font)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45, **axis_font)
    plt.yticks(tick_marks, classes, **axis_font)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label', **axis_font)
    plt.xlabel('Predicted label', **axis_font)
    plt.tight_layout()

def scaling_operation(do_scaling, X, y):
    """
    :param do_scaling: True or False
    :param X:
    :param y:
    :return: Scaled total train data
    """
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10)

    if do_scaling:

        scaler_x_minmax = MinMaxScaler()

        scaler_x_minmax.fit(X_train)
        X_train_scaled = scaler_x_minmax.transform(X_train)

        X_test_scaled = scaler_x_minmax.transform(X_test)

        scaler_y_minmax = MinMaxScaler(feature_range=(0.1,0.9))
        scaler_y_minmax.fit(y_train)
        y_train_scaled = scaler_y_minmax.transform(y_train)

        return scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax

    else:

        return None, X_train, X_test, y_train, y_test, None

def scaling_operation_classifier(do_scaling, X, y):
    """
    :param do_scaling: True or False
    :param X: Scaling operation at input features
    :param y: Scaling operation at target lables
    :return: Scaled total train, test data
    """

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    if do_scaling:
        # Scaling with minmax scaler
        scaler_x_minmax = MinMaxScaler()
        # Fit the data
        scaler_x_minmax.fit(X_train)
        # Transform the data
        X_train_scaled = scaler_x_minmax.transform(X_train)

        X_test_scaled = scaler_x_minmax.transform(X_test)

        return X_train_scaled, X_test_scaled, y_train, y_test, scaler_x_minmax
    else:
        # Data will not scaled
        return X_train, X_test, y_train, y_test, None

def train_model(X, y, use_keras=False, params=None):
    """
    :param X: Give your feature data
    :param y: Select your target lables
    :param use_keras: True or False
    :param params: Set hyperparameter tuning
    :return: trained model Predictor
    """

    if params == None:
        num_layers = 2
        num_neurons = 130
        activation = 'relu'
        learning_rate_init = 1e-4
        n_epochs = 10
        batch_size = 100
        #noise_X = 0
        #noise_y = 0
    else:
        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation = params['activation']
        learning_rate_init = params['learning_rate_init']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']
        #noise_X = params['noise_X']
        #noise_y = params['noise_y']

    # Adding noise at X and y label
    #X = X * (1 + (noise_X * np.random.normal(size=(X.shape[0], X.shape[1]))))
    #y = y * (1 + (noise_y * np.random.normal(size=(y.shape[0], y.shape[1]))))

    if use_keras:
        keras.backend.clear_session()

        # Select an Optimizer
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)

        # Initialize a sequential / feed forward model
        regressor = Sequential()

        # Add input at first hidden layer
        regressor.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        # Add subsequent hidden layer
        for _ in range(num_layers - 1):
            regressor.add(Dense(units=num_neurons,
                                activation=activation
                                )
                          )

        # Add Output Layer
        regressor.add(Dense(units=y.shape[1], activation='relu'))

        # Complile the regressor
        regressor.compile(optimizer=optimizer, loss='mse', metrics=['accuracy'])

        # Set up for validation
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train the model, varbose = 1(Print the result in the screen during the training)
        history = regressor.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_data=(X_test, y_test),
                                verbose=0)

        # Plot the result
        # Set the font dictionaries (for plot title and axis titles)
        title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                      'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
        axis_font = {'fontname': 'Arial', 'size': '24'}

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)

        # Set the tick labels font
        for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
            label.set_fontname('Arial')
            label.set_fontsize(24)

        ax_loss.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        ax_loss.set_title('num_layers:' + str(num_layers) + ',num_neurons:' + str(num_neurons) +
                          ', activation:' + str(activation) + ', learning_rate_init:' + str(learning_rate_init) +
                          ', batch_size:' + str(batch_size), **title_font)
        ax_loss.set_ylabel('Loss(MAE)', **axis_font)
        ax_loss.set_xlabel('Number of Epochs', **axis_font)
        # ax_loss.grid()
        plt.show()



    else:

        regressor = regr()
        regressor.fit(X, y)
        """
        # When bagging regressor activated
        regressor = regr()
        regressor = BaggingRegressor(regressor, n_estimators=5)

        for _ in range(n_epochs):
            regressor.fit(X, y)
        """

    return regressor

def train_model_calculator(X, y, use_keras=False, params=None):
    """
    :param X: Give your feature data
    :param y: Select your target lables
    :param use_keras: True or False
    :param params: Set hyperparameter tuning
    :return: trained model calculator
    """

    if params == None:

        num_layers = 2
        num_neurons = 130
        activation = 'relu'
        learning_rate_init = 1e-4
        n_epochs = 10
        batch_size = 100
        #noise_X = 0
        #noise_y = 0
    else:
        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation = params['activation']
        learning_rate_init = params['learning_rate_init']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']
        #noise_X = params['noise_X']
        #noise_y = params['noise_y']

    # Adding noise at X and y label
    #X = X * (1 + (noise_X * np.random.normal(size=(X.shape[0], X.shape[1]))))
    #y = y * (1 + (noise_y * np.random.normal(size=(y.shape[0], y.shape[1]))))

    if use_keras:
        keras.backend.clear_session()

        # Select an Optimizer
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)

        # Initialize a sequential/ feed forward model
        regressor = Sequential()

        # Add input and first hidden layer
        regressor.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        # Add subsequent hidden layer
        for _ in range(num_layers - 1):
            regressor.add(Dense(units=num_neurons,
                                activation=activation
                                )
                          )

        # Add Output Layer
        regressor.add(Dense(units=y.shape[1], activation='relu'))

        # Complile the regressor
        regressor.compile(optimizer=optimizer, loss='mse', metrics=['accuracy'])

        # Set up for validation during the training
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train the model, varbose = 1(Print the result in the screen during the training)
        history = regressor.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_data=(X_test, y_test),
                                verbose=0)

        # Plot the results
        # Set the font dictionaries (for plot title and axis titles)
        title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                      'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
        axis_font = {'fontname': 'Arial', 'size': '24'}

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)

        # Set the tick labels font
        for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
            label.set_fontname('Arial')
            label.set_fontsize(24)

        ax_loss.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        ax_loss.set_title('num_layers:' + str(num_layers) + ',num_neurons:' + str(num_neurons) +
                          ', activation:' + str(activation) + ', learning_rate_init:' + str(learning_rate_init) +
                          ', batch_size:' + str(batch_size), **title_font)
        ax_loss.set_ylabel('Loss(MAE)', **axis_font)
        ax_loss.set_xlabel('Number of Epochs', **axis_font)
        # ax_loss.grid()
        plt.show()



    else:

        regressor = regr()
        regressor.fit(X, y)
        """
        regressor = regr()
        regressor = BaggingRegressor(regressor, n_estimators=5)

        for _ in range(n_epochs):
            regressor.fit(X, y)
        """

    return regressor

def train_model_classifier(X, y, use_keras=False, params=None):
    """
    :param X: Give your feature data
    :param y: Select your target lables
    :param use_keras: True or False
    :param params: Set hyperparameter tuning
    :return: trained model classifier
    """

    if params == None:

        num_layers = 2
        num_neurons = 130
        activation = 'relu'
        learning_rate_init = 1e-4
        n_epochs = 10
        batch_size = 100
        #noise_X = 0
        #noise_y = 0
    else:
        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation = params['activation']
        learning_rate_init = params['learning_rate_init']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']
        #noise_X = params['noise_X']
        #noise_y = params['noise_y']

    # Adding noise at X and y label
    #X = X * (1 + (noise_X * np.random.normal(size=(X.shape[0], X.shape[1]))))
    #y = y * (1 + (noise_y * np.random.normal(size=(y.shape[0], y.shape[1]))))

    if use_keras:
        keras.backend.clear_session()

        # Select an Optimizer
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)
        # optimizer = keras.optimizers.Adamax(lr=learning_rate_init)

        # Initialize a sequential/ feed forward model
        classifier = Sequential()

        # Add input and first hidden layer
        classifier.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        # Add subsequent hidden layer
        for _ in range(num_layers - 1):
            classifier.add(Dense(units=num_neurons,
                                activation=activation
                                )
                          )

        # Add output layer
        classifier.add(Dense(units=y.shape[1], activation='sigmoid'))

        # Complile the regressor
        classifier.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])

        # Set up for validation
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train the model, varbose = 1(Print the result in the screen during the training)
        history = classifier.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_data=(X_test, y_test),
                                verbose=0)

        # Plot the results
        # Set the font dictionaries (for plot title and axis titles)
        title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                      'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
        axis_font = {'fontname': 'Arial', 'size': '24'}

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)

        # Set the tick labels font
        for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
            label.set_fontname('Arial')
            label.set_fontsize(24)

        ax_loss.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        ax_loss.set_title('num_layers:' + str(num_layers) + ',num_neurons:' + str(num_neurons) +
                          ', activation:' + str(activation) + ', learning_rate_init:' + str(learning_rate_init) +
                          ', batch_size:' + str(batch_size), **title_font)
        ax_loss.set_ylabel('Loss(MAE)', **axis_font)
        ax_loss.set_xlabel('Number of Epochs', **axis_font)
        # ax_loss.grid()
        plt.show()


    else:
        classifier = clf()
        classifier.fit(X, y)

    return classifier

def get_metrics(true, predicted):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Regression Score (mape, mae, rmse, evs, r2_s)
    """
    error = true - predicted
    pe = (error) / true

    mape = np.mean(np.abs(pe)) * 100
    print('Mean Absolute Percentage Error : {}'.format(mape))

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    evs = explained_variance_score(true, predicted)
    print('Explained Variance Score: {}'.format(evs))

    r2_s = r2_score(y_true=true, y_pred=predicted)
    print('R2 Score : {}'.format(r2_s))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)


    plt.hist(error,50)
    #plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of Observations', **axis_font)
    plt.title('Histogram of Error', **title_font)
    #plt.grid()
    plt.show()

    return mape, mae, rmse, evs, r2_s

def get_metrics_clf(true, predicted):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Classification Score(F1 Score)
    """

    f1s = f1_score(true, predicted)
    print('F1 Score: ',f1s)

    return None

def plot_results(true, predicted, scores):
    """

    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """
    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title("MAPE:" + str(round(scores[0], 3)) + " MAE:" + str(round(scores[1], 3)) + " RMSE:" + str(
            round(scores[2], 3)) + " EVS:" + str(round(scores[3], 3)), **title_font)
    ax_loss.set_ylabel('Predicted', **axis_font)
    ax_loss.set_xlabel('Number of Observations', **axis_font)
    # ax_loss.grid()
    plt.legend(('Actual', 'Predicted'), loc='upper right')
    plt.show()

def main():
    """

    :return: Hourly Forecasting Prediction Model
    """

########################################################################################################################
    # Data Controlling Panel
########################################################################################################################

    # Set limit for Predictor Model at total precipitation data
    lower_limit_predictor = -1
    upper_limit_predictor = 3

    # scaling operation for Prediction Model
    do_scaling_pred = True

    # scaling operation for Classification Model
    do_scaling_clf = False

    # Set limit for Calculator Model at total precipitation data
    lower_limit_calculator = -1
    upper_limit_calculator = 3

    # scaling operation for Calculation Model
    do_scaling_calc = True

    # Select Predicting hour index (Total Precipitation)
    hour_selection = 16


########################################################################################################################
    # Operations for Prediction Model
########################################################################################################################


    # Set the limit for data and which hour should be predicted
    X, y = data_selection_predictor(upper_limit_predictor, lower_limit_predictor,hour_selection)

    # Create a  empty dictionary to store the train model details
    dict_of_models = {}

    # Name the list all of input features
    list_of_features = ['relative_humidity', 'mean_sea_level_pressure', 'low_cloud_cover', 'sunshine_duration',
                        'shortwave_radiation', 'wind_direction_10m_above_gnd', 'wind_speed']
    # Split the data for training and testing
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    j = 0
    i = 0

    # Make an empty list for storing the predictions at 16h for each feature
    pred = []

    # Train one model for each feature and store the trained model and the predictions in the dictionary
    for feature in list_of_features:

        # Select the indices for each feature from 0h to 15h from the main input X
        new_x_train = X_train[:, j:j + 16]
        new_x_test = X_test[:, j:j + 16]
        # Select the indices for each feature at 16h from the main input y
        new_y_train = y_train[:, i].reshape(-1, 1)
        new_y_test = y_test[:, i].reshape(-1, 1)

        print('\n\n Predictor Feature Name: {}, X_Train = {}, y_train = {}'
              ' '.format(feature, new_x_train.shape, new_y_train.shape))

        if do_scaling_pred:

            # Do the scaling operations with MinMax scaler
            scaler_x_minmax_pred = MinMaxScaler()
            scaler_x_minmax_pred.fit(new_x_train)
            X_train_scaled = scaler_x_minmax_pred.transform(new_x_train)
            X_test_scaled = scaler_x_minmax_pred.transform(new_x_test)


            scaler_y_minmax = MinMaxScaler(feature_range=(0.1,0.9))
            scaler_y_minmax.fit(new_y_train)
            y_train_scaled = scaler_y_minmax.transform(new_y_train)
        else:
            # Data is not scaled
            X_train_scaled = new_x_train
            X_test_scaled = new_x_test
            y_train_scaled = new_y_train

        # Set the feature names every iterations
        dict_of_models[feature] = {}
        # Hyper parameter tuning for Prediction Model
        param = {}
        param['num_layers'] = 2
        param['num_neurons'] = 130
        param['activation'] = 'relu'
        param['learning_rate_init'] = 1e-4
        param['n_epochs'] = 10
        param['batch_size'] = 10
        #param['noise_X'] = 0
        #param['noise_y'] = 0

        # Training model and storing the trained model in the dictionary
        dict_of_models[feature]['model'] = train_model(X_train_scaled, y_train_scaled, use_keras=True,
                                                       params=param)

        # Inverse Scaling with Minmax scaler
        if do_scaling_pred:
            # Predict the feature with scaled data
            predicted_actual_scaled = dict_of_models[feature]['model'].predict(X_test_scaled)
            # Remove Scaling after prediction
            predicted_actual = scaler_y_minmax.inverse_transform(predicted_actual_scaled.reshape(-1, 1))
        else:
            # Predict the feature with original data
            predicted_actual = dict_of_models[feature]['model'].predict(new_x_test)

        # Store the predictions in the list declared before
        pred.append(predicted_actual[:, 0])

        # Compare predicted value with true value
        dict_of_models[feature]['scr'] = get_metrics(new_y_test, predicted_actual)

        # Plotting all feature prediction results
        plot_results(new_y_test, predicted_actual, dict_of_models[feature]['scr'])
        j = j + 16
        i = i + 1

    # Changing the data type to np array
    dict_of_models['predicted_features'] = np.array(pred).T

    '''
    Dump the model when not using keres.
    Rainfall data at 16h which was stored in the first prediction model features last columns
    '''
    #dict_of_models['model_1_rainfall_data'] = y_test[:, -1]
    #print("Dumping dictionary to hardisk")
    #dump(dict_of_models, 'dict_of_models.pkl')
    #print("Loading dictionary from hardisk")
    #dict_of_models = load('dict_of_models.pkl')


########################################################################################################################
    # Operations for Classification
########################################################################################################################

    # Set the limit for data and which hour(hour_selection) will be predicted
    X_clf, y_clf = data_selection_classifier(hour_selection)

    X_train_scaled_clf, X_test_scaled_clf, y_train_clf,\
    y_test_clf, scaler_x_minmax_clf = scaling_operation_classifier(do_scaling_clf, X_clf, y_clf)

    # Hyper parameter tuning for Classification Model
    param = {}
    param['num_layers'] = 2
    param['num_neurons'] = 130
    param['activation'] = 'relu'
    param['learning_rate_init'] = 1e-4
    param['n_epochs'] = 100
    param['batch_size'] = 300
    #param['noise_X'] = 0.5
    #param['noise_y'] = 0

    model_classifier = train_model_classifier(X_train_scaled_clf, y_train_clf,
                                   use_keras=False,
                                   params=param)

    # Predicted Classifier output is scaled
    predicted_classifier = np.round(model_classifier.predict(X_test_scaled_clf))

    print('\n\n Metrics for Classifier')
    get_metrics_clf(y_test_clf, predicted_classifier)

    # Set the target label for confusion matrix
    target_label = np.unique(y_test_clf)

    '''
    # Confusion matrix at trained data
    cnf_matrix = confusion_matrix(y_train_clf, np.round(model_classifier.predict(X_train_scaled_clf)))
    '''

    # Confusion matrix at test data
    cnf_matrix = confusion_matrix(y_test_clf, predicted_classifier)

    # Plot confusion matrix
    plot_confusion_matrix(cnf_matrix, classes = target_label,
                          title='Confusion matrix for Classification')

    plt.show()



########################################################################################################################
    # Operations for Calculator
########################################################################################################################

    X_calculator, y_calculator = data_selection_calculator(upper_limit_calculator,
                                                           lower_limit_calculator,
                                                           hour_selection)
    '''
    #Kernel Principle Component Analysis
    
    #kpca_calculator = KernelPCA()
    #kpca_calculator.fit(X_calculator)
    #X_calculator = kpca_calculator.transform(X_calculator)
    '''
    scaler_y_minmax_calculator, \
    X_train_scaled_calculator_data, X_test_scaled_calculator_data, \
    y_train_scaled_calculator_data, y_test_calculator_data, \
    scaler_x_minmax = scaling_operation(do_scaling_calc, X_calculator, y_calculator)

    # Hyper parameter tuning for Classification Model
    param = {}
    param['num_layers'] = 9
    param['num_neurons'] = 900
    param['activation'] = 'relu'
    param['learning_rate_init'] = 1e-4
    param['n_epochs'] = 60
    param['batch_size'] = 100
    # param['noise_X'] = 0.5
    # param['noise_y'] = 0


    model_calculator = train_model_calculator(X_train_scaled_calculator_data, y_train_scaled_calculator_data,
                                   use_keras=True,
                                   params=param)



    if do_scaling_calc:
        # Predict hourly rainfall with feature scaled data
        predicted_scaled_calculator = model_calculator.predict(X_test_scaled_calculator_data)
        # Remove Scaling after prediction
        predicted_actual_calculator = scaler_y_minmax_calculator.inverse_transform(
            predicted_scaled_calculator.reshape(-1, 1))

    else:
        # Predict hourly rainfall with feature actual data
        predicted_actual_calculator = model_calculator.predict(X_test_scaled_calculator_data)

    print('\n\nMetrics for Calculator')
    scr = get_metrics(y_test_calculator_data, predicted_actual_calculator)
    plot_results(y_test_calculator_data, predicted_actual_calculator, scr)

#########################################################################################################################
    # Operations for Combine Model
########################################################################################################################

    if do_scaling_clf == True and do_scaling_calc == True:

        # Original predicted feature data
        X_pred = dict_of_models['predicted_features']

        # Classifier Operation
        X_pred_minmax_scaled_clf = scaler_x_minmax_clf.transform(X_pred)

        #Predict using trained classifierk, ANN predict in between 0 to 1 with classifier
        rainfall_predicted = np.round(model_classifier.predict(X_pred_minmax_scaled_clf)).astype(float).reshape(-1, 1)

        # Pass samples for which classifier prediction = 1
        non_zero_pred_idx = np.where(rainfall_predicted == 1)[0]

        # Calculator Operation
        # Data pre processing
        X_pred_minmax_scaled_calc = scaler_x_minmax.transform(X_pred)

        # Predict using trained calculator
        non_zero_pred_scaled = model_calculator.predict(X_pred_minmax_scaled_calc[non_zero_pred_idx, :]).reshape(-1, 1)

        # Data post processing - decoding data or un-scaling data to actual form
        non_zero_pred = scaler_y_minmax_calculator.inverse_transform(non_zero_pred_scaled)

        rainfall_predicted[non_zero_pred_idx, :] = non_zero_pred

        '''
        #Scatter Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        # plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)
        xc = np.arange(len(y_test[:, -1]))
        plt.scatter(xc, y_test[:, -1], label='Rainfall at 16 Hour')
        plt.scatter(xc, rainfall_predicted[:, 0], label='Predicted Rainfall at 16h ')
        plt.legend()
        plt.show()
        '''

        #Line Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)

    elif do_scaling_clf == False and do_scaling_calc == False:

        # Original predicted feature data
        X_pred = dict_of_models['predicted_features']

        # Classifier Operation
        # Predict using trained classifierk, ANN predict in between 0 to 1 with classifier
        rainfall_predicted = np.round(model_classifier.predict(X_pred)).astype(float).reshape(-1, 1)

        # Pass samples for which classifier prediction = 1
        non_zero_pred_idx = np.where(rainfall_predicted == 1)[0]

        # Calculator Operation
        # Predict using trained calculator
        non_zero_pred = model_calculator.predict(X_pred[non_zero_pred_idx, :]).reshape(-1, 1)

        rainfall_predicted[non_zero_pred_idx, :] = non_zero_pred

        '''
        # Scatter Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        # plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)
        xc = np.arange(len(y_test[:, -1]))
        plt.scatter(xc, y_test[:, -1], label='Rainfall at 16 Hour')
        plt.scatter(xc, rainfall_predicted[:, 0], label='Predicted Rainfall at 16h ')
        plt.legend()
        plt.show()
        '''

        # Line Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)

    elif do_scaling_clf == True and do_scaling_pred == False:

        # Original predicted feature data
        X_pred = dict_of_models['predicted_features']

        # Classifier Operation
        X_pred_minmax_scaled_clf = scaler_x_minmax_clf.transform(X_pred)

        # Predict using trained classifier, ANN predict in between 0 to 1 with classifier
        rainfall_predicted = np.round(model_classifier.predict(X_pred_minmax_scaled_clf)).astype(float).reshape(-1, 1)

        # Pass samples for which classifier prediction = 1
        non_zero_pred_idx = np.where(rainfall_predicted == 1)[0]

        # Calculator Operation
        # Predict using trained calculator
        non_zero_pred = model_calculator.predict(X_pred[non_zero_pred_idx, :]).reshape(-1, 1)

        rainfall_predicted[non_zero_pred_idx, :] = non_zero_pred

        '''
        # Scatter Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        # plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)
        xc = np.arange(len(y_test[:, -1]))
        plt.scatter(xc, y_test[:, -1], label='Rainfall at 16 Hour')
        plt.scatter(xc, rainfall_predicted[:, 0], label='Predicted Rainfall at 16h ')
        plt.legend()
        plt.show()
        '''

        # Line Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)

    elif do_scaling_clf == False and do_scaling_calc == True:

        # Original predicted feature data
        X_pred = dict_of_models['predicted_features']

        # Classifier Operation
        # Predict using trained classifierk, ANN predict in between 0 to 1 with classifier
        rainfall_predicted = np.round(model_classifier.predict(X_pred)).astype(float).reshape(-1, 1)

        # Pass samples for which classifier prediction = 1
        non_zero_pred_idx = np.where(rainfall_predicted == 1)[0]

        # Calculator Operation
        # Data pre-processing data
        X_pred_minmax_scaled_calc = scaler_x_minmax.transform(X_pred)

        # Predict using trained calculator
        non_zero_pred_scaled = model_calculator.predict(X_pred_minmax_scaled_calc[non_zero_pred_idx, :]).reshape(-1, 1)

        # Post processing - decoding data or un-scaling data to actual form
        non_zero_pred = scaler_y_minmax_calculator.inverse_transform(non_zero_pred_scaled)

        rainfall_predicted[non_zero_pred_idx, :] = non_zero_pred

        '''
        # Scatter Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        # plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)
        xc = np.arange(len(y_test[:, -1]))
        plt.scatter(xc, y_test[:, -1], label='Rainfall at 16 Hour')
        plt.scatter(xc, rainfall_predicted[:, 0], label='Predicted Rainfall at 16h ')
        plt.legend()
        plt.show()
        '''

        # Line Plot
        print('\n\nMetrics for Combined Rainfall Forecasting')
        scr = get_metrics(y_test[:, -1], rainfall_predicted[:, 0])
        plot_results(y_test[:, -1], rainfall_predicted[:, 0], scr)

    else:
        pass

if __name__ == "__main__":
    main()
