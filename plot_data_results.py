from matplotlib import pyplot as plt
import matplotlib.image as image
import matplotlib.pyplot as plt


# Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname': 'Arial', 'size': '20', 'color': 'black', 'weight': 'normal',
              'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
axis_font = {'fontname': 'Arial', 'size': '18'}


plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Confusion_Matrix/confuaion_matrix_dataset_with_biased.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Confusion Matrix With Biased Dataset')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Confusion_Matrix/confuaion_matrix_dataset_wihout_biased.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Confusion Matrix Without Biased Dataset')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.show()
"""
###############################################################################################
#Training Loss
###############################################################################################

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Training_Loss/relative_humidity_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Relative Humidity')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Training_Loss/mean_sea_level_pressure_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Mean Sea Level Pressure')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Training_Loss/low_cloud_cover_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Low Cloud Cover')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Training_Loss/sunshine_duration_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Sunshine Duration')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Training_Loss/shortwave_radiation_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Shortwave Radiation')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Training_Loss/wind_direction_10m_above_gnd_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Wind Direction 10m Above Ground')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Training_Loss/wind_speed_900mb_training_loss.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Shortwave Radiation')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


###############################################################################################
# Histogram of Error
###############################################################################################

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Histogram_Error/relative_humidity_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Relative Humidity')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Histogram_Error/mean_sea_level_pressure_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Mean Sea Level Pressure')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Histogram_Error/low_cloud_cover_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Low Cloud Cover')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Histogram_Error/sunshine_duration_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Sunshine Duration')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Histogram_Error/shortwave_radiation_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Shortwave Radiation')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Histogram_Error/wind_direction_10m_above_gnd_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Wind Direction 10m Above Ground')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Histogram_Error/wind_speed_900mb_hist_error.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Wind Speed 900mb')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

###############################################################################################
#Prediction Result
###############################################################################################

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Prediction/relative_humidity_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Relative Humidity')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Prediction/mean_sea_level_pressure_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Mean Sea Level Pressure')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Prediction/low_cloud_cover_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Low Cloud Cover')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Prediction/sunshine_duration_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Sunshine Duration')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Prediction/shortwave_radiation_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Shortwave Radiation')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Prediction/wind_direction_10m_above_gnd_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Wind Direction 10m Above Ground')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Prediction/wind_speed_900mb_pred_results.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Wind Speed 900mb')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

###############################################################################################
# Confusion Matrix Result
###############################################################################################
plt.subplot(121)
#plt.subplot(211)
img = image.imread('Graph_Results/Confusion_Matrix/confuaion_matrix_dataset_with_biased.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Relative Humidity')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(122)
#plt.subplot(212)
img = image.imread('Graph_Results/Confusion_Matrix/confuaion_matrix_dataset_wihout_biased.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Mean Sea Level Pressure')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


"""